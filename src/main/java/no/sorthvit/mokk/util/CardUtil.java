package no.sorthvit.mokk.util;

public class CardUtil {
    private CardUtil() {

    }

    public static String printCardValue(final Integer value){
        switch (value){
            case 11:
                return "J";
            case 12:
                return "D";
            case 13:
                return "K";
            case 14:
                return "A";
            case 15:
                return "2";
            default:
                return value.toString();
        }
    }

    public static char printCardColor(final int value){
        char color = ' ';
        switch (value){
            case 0:
                color = '\u2660';
                break;
            case 1:
                color = '\u2666';
                break;
            case 2:
                color = '\u2663';
                break;
            case 3:
                color = '\u2764';
                break;
        }
        return color;
    }
}
