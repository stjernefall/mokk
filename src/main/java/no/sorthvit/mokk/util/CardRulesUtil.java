package no.sorthvit.mokk.util;

import no.sorthvit.mokk.game.deck.Card;

import java.util.List;

public class CardRulesUtil {

    public static boolean cardsCanBeUsed(final Card topCard, final List<Card> toEvaluate) {
        if (null != toEvaluate && !toEvaluate.isEmpty()) {
            if (toEvaluate.size() != 2 && toEvaluate.size() < 5){
                final Card tempCard = toEvaluate.get(0);
                for (final Card cardToCheck : toEvaluate) {
                    if (cardToCheck.getValue() != tempCard.getValue()){
                        return false;
                    }
                }
                if (tempCard.isAce()){
                    return aceRules(topCard);
                }
                return cardValueCalculation(topCard, tempCard);
            }
        }
        return false;
    }

    public static boolean cardCanBeUsed(final Card topCard, final Card toEvaluate) {
        if (null != toEvaluate && toEvaluate.getValue() == 14){
            return aceRules(topCard);
        } else if (null != toEvaluate && toEvaluate.getValue() == 10){
            return tenRules(topCard);
        }
        return cardValueCalculation(topCard, toEvaluate);
    }

    private static boolean cardValueCalculation(final Card topCard, final Card toEvaluate){
        return toEvaluate != null && (topCard == null || topCard.getValue() <= toEvaluate.getValue());
    }

    public static boolean aceRules(final Card topCard){
        return null == topCard || topCard.getValue() > 10 && topCard.getValue() != 15;
    }

    public static boolean tenRules(final Card topCard){
        return null == topCard || topCard.getValue() <= 9 && topCard.getValue() != 15;
    }

}
