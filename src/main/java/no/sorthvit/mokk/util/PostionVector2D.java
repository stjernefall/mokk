package no.sorthvit.mokk.util;

public class PostionVector2D {
    private float x;
    private float y;
    private boolean drawHorizontal;

    public PostionVector2D(final float x, final float y, boolean drawHorizontal) {
        this.x = x;
        this.y = y;
        this.drawHorizontal = drawHorizontal;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public boolean drawHorizontal() {
        return drawHorizontal;
    }
}
