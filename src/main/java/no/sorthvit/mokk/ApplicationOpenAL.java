package no.sorthvit.mokk;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.ALCapabilities;
import org.springframework.core.io.ClassPathResource;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.ALC10.*;

public class ApplicationOpenAL {

    public static void main(String[] args) throws IOException, InterruptedException, UnsupportedAudioFileException {
        AudioInputStream stream = AudioSystem.getAudioInputStream(new ClassPathResource("sound.wav").getInputStream());
        AudioFormat format = stream.getFormat();
        int alFormat = -1;
        if (format.getChannels() == 1) {
            if (format.getSampleSizeInBits() == 8) {
                alFormat = AL_FORMAT_MONO8;
            } else if (format.getSampleSizeInBits() == 16) {
                alFormat = AL_FORMAT_MONO16;
            }
        } else if (format.getChannels() == 2) {
            if (format.getSampleSizeInBits() == 8) {
                alFormat = AL_FORMAT_STEREO8;
            } else if (format.getSampleSizeInBits() == 16) {
                alFormat = AL_FORMAT_STEREO16;
            }
        }
        if (alFormat == -1) throw new RuntimeException("can't handle format");
        byte[] byteArray = new byte[stream.available()];
        stream.read(byteArray);
        ByteBuffer audioBuffer = BufferUtils.createByteBuffer(byteArray.length);
        audioBuffer.put(byteArray);
        stream.close();
        audioBuffer.flip();

        long device = alcOpenDevice((java.lang.CharSequence) null);
        long context = alcCreateContext(device, (int[]) null);
        alcMakeContextCurrent(context);
        ALCCapabilities alcCapabilities = ALC.createCapabilities(device);
        ALCapabilities alCapabilities = AL.createCapabilities(alcCapabilities);
        int buffer = alGenBuffers();
        alBufferData(buffer, alFormat, audioBuffer, (int) format.getSampleRate());
        int source = alGenSources();
        alSourcei(source, AL_BUFFER, buffer);
        alSourcePlay(source);
        Thread.sleep(10000);
        alcCloseDevice(device);
    }
}
