package no.sorthvit.mokk.game.stats;

import no.sorthvit.mokk.game.player.Player;

import java.util.Map;

public class Stats {
    Integer firstPlaceId = null;
    Integer secondplaceId = null;
    Integer thirdPlaceId = null;
    Integer fourthPlaceId = null;

    private int numberOfRounds;

    public Stats(final Map<Integer, Integer> playerStatsEndPostions, final int numberOfRounds) {
        for (final Map.Entry<Integer, Integer> entry : playerStatsEndPostions.entrySet()) {
            if (entry.getValue() == 0){
                firstPlaceId = entry.getKey();
            } else if(entry.getValue() == 1){
                secondplaceId = entry.getKey();
            }else if(entry.getValue() == 2){
                thirdPlaceId = entry.getKey();
            }else {
                fourthPlaceId = entry.getKey();
            }
        }
        this.numberOfRounds = numberOfRounds;
    }

    public Integer getFirstPlaceId() {
        return firstPlaceId;
    }

    public Integer getSecondplaceId() {
        return secondplaceId;
    }

    public Integer getThirdPlaceId() {
        return thirdPlaceId;
    }

    public Integer getFourthPlaceId() {
        return fourthPlaceId;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "firstPlaceId=" + firstPlaceId +
                ", secondplaceId=" + secondplaceId +
                ", thirdPlaceId=" + thirdPlaceId +
                ", fourthPlaceId=" + fourthPlaceId +
                ", numberOfRounds=" + numberOfRounds +
                '}';
    }
}
