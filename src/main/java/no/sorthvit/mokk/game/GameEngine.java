package no.sorthvit.mokk.game;

import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.game.deck.ClosedDeck;
import no.sorthvit.mokk.game.deck.OpenDeck;
import no.sorthvit.mokk.game.player.HumanPlayerImpl;
import no.sorthvit.mokk.game.player.NonPlayer;
import no.sorthvit.mokk.game.player.Player;
import no.sorthvit.mokk.game.player.PlayerBase;
import no.sorthvit.mokk.util.PostionVector2D;

import java.util.*;

import static no.sorthvit.mokk.util.CardRulesUtil.*;

public class GameEngine {

    private Random random;
    private int numberOfRounds;
    private List<PlayerBase> players;
    private Map<Integer, PlayerBase> playersTurnOrder;
    private ClosedDeck closedDeck;
    private OpenDeck openDeck;
    private Player humanPlayer;
    private int currentPlayer;
    private final List<PostionVector2D> positionList = new ArrayList<>();

    public GameEngine(final List<PlayerBase> players) throws Exception {
        if (players.size() < 2 || players.size() > 4){
            throw new Exception("Må være flere enn 2 spillere og færre en 4 spillere");
        }
        this.random = new Random();
        this.players = players;
        this.closedDeck = new ClosedDeck(new ArrayList<>());
        this.openDeck = new OpenDeck();
        this.playersTurnOrder = new HashMap<>();

        // Player positions //
//        positionList.add(new PostionVector2D(0.0f, -0.6f, true));
        positionList.add(new PostionVector2D(-0.1f, -0.3f, true));
        positionList.add(new PostionVector2D(-0.8f, +0.25f, false));
        positionList.add(new PostionVector2D(-0.1f, +0.8f, true));
        positionList.add(new PostionVector2D(+0.5f, +0.25f, false));
    }

    public void initNewGame() {
        this.numberOfRounds = 0;
        closedDeck.clear();
        openDeck.clear();
        currentPlayer = 0;
        humanPlayer = null;
        playersTurnOrder.clear();

        closedDeck = new ClosedDeck(generateAllPlayableCards());
        openDeck = new OpenDeck();
        Collections.shuffle(players, random);

        for (int i = 0; i < getAllPlayers().size(); i++) {
            final PlayerBase player = getAllPlayers().get(i);
            playersTurnOrder.put(i, player);
            if (player instanceof HumanPlayerImpl) {
                this.humanPlayer = player;
            }
            if (!(player instanceof NonPlayer)) {
                player.setPositionX(positionList.get(i).getX());
                player.setPositionY(positionList.get(i).getY());
                player.setDrawHorizontal(positionList.get(i).drawHorizontal());
                player.giveCards(drawCardsForPlayers());
            }
        }
    }

    public ClosedDeck getClosedDeck() {
        return closedDeck;
    }

    public OpenDeck getOpenDeck() {
        return openDeck;
    }

    public boolean isGameFinished() {
        int finishedPlayers = 0;
        for (final Player player : players) {
            if (player.isFinished()) {
                finishedPlayers = finishedPlayers + 1;
            }
        }
        return (players.size() - 1) == finishedPlayers;
    }

    private List<Card> generateAllPlayableCards() {
        final List<Card> playableCards = new ArrayList<>();
        for (int allValues = 3; allValues < 16; allValues++) {
            for (int allColors = 0; allColors < 4; allColors++) {
                playableCards.add(new Card(allValues, allColors, false));
            }
        }
        return playableCards;
    }

    private List<Card> drawCardsForPlayers() {
        final List<Card> cardList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            cardList.add(closedDeck.drawCard());
        }
        return cardList;
    }

    public ArrayList<Player> getActivePlayers() {
        final ArrayList<Player> activePlayers = new ArrayList<>();
        for (final Player player : getAllPlayers()) {
            if (!player.isFinished()) {
                activePlayers.add(player);
            }
        }
        return activePlayers;
    }

    public boolean takeAChange(final Player player) {
        final Card chanceCard = getClosedDeck().drawCard();
        System.out.println(player.name() + " tar en sjanse og trekker " + chanceCard);
        if (cardCanBeUsed(getOpenDeck().topCard(), chanceCard)) {
            System.out.println(player.name() + " greide sjansen og slipper å trekke");
            playCard(player, Collections.singletonList(chanceCard), true);
            return (aceRules(chanceCard) || tenRules(chanceCard)) && !getOpenDeck().hasCards();
        } else {
            System.out.println(player.name() + " feilet sjansen og må trekke " + (getOpenDeck().totalCards() + 1) + " kort");
            player.giveCards(getOpenDeck().drawAllCards());
            player.giveCard(chanceCard);
        }
        return false;
    }

    //todo: drawCards til man har 5 kort //
    public void drawCard(final Player player) {
        if (player.totalCards() < 5) {
            while (player.totalCards() < 5 && getClosedDeck().hasCards()) {
                final Card cardToDraw = getClosedDeck().drawCard();
                if (null == cardToDraw) {
                    System.out.println("Ikke flere kort å trekke.");
                } else {
                    System.out.println(player.name() + " trekker nytt kort: " + cardToDraw);
                    player.giveCard(cardToDraw);
                }
            }
        } else {
            System.out.println(player.name() + " trekker ikke nytt kort fordi han har " + player.totalCards() + " kort");
        }
    }

    public boolean playCard(final Player player, final List<Card> cards, final boolean isChanceCard) {
        System.out.println(player.name() + " legger på bordet: " + cards);
        final Card cardOnTop = getOpenDeck().addCard(cards);
        if (!isChanceCard) {
            player.removeCards(cards);
        }
        if (null == cardOnTop) {
            System.out.println("Bunken slått ut, " + player.name() + " sin tur igjen.");
            return !player.isFinished();
        }
        return false;
    }

    public boolean checkIfPlayerMustDraw(final Player currentPlayer) {
        System.out.println(currentPlayer.name() + "'s hand: " + currentPlayer.showHand() + " antall " + currentPlayer.totalCards());
        if (getOpenDeck().hasCards() &&
                getOpenDeck().totalCards() == 1 &&
                getOpenDeck().topCard().isEreaseCard()) {
            System.out.println(currentPlayer.name() + " må trekke inn kortet: " + getOpenDeck().topCard());
            currentPlayer.giveCards(getOpenDeck().drawAllCards());
            return true;
        }
        return false;
    }

    public List<PlayerBase> getAllPlayers() {
        return players;
    }

    public Card getTopCard() {
        return openDeck.topCard();
    }

    public Player getHumanPlayer() {
        return humanPlayer;
    }

    public void nextPlayer() {
        if (currentPlayer + 1 == playersTurnOrder.size()) {
            currentPlayer = 0;
        } else {
            currentPlayer = currentPlayer + 1;
        }
    }

    public Player getCurrentPlayer() {
        return playersTurnOrder.get(this.currentPlayer);
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public void increaseNumberOfRounds(final Player currentPlayer) {
        if (playersTurnOrder.get(playersTurnOrder.size() - 1).getId() == currentPlayer.getId()) {
            this.numberOfRounds = numberOfRounds + 1;
        }
    }
}