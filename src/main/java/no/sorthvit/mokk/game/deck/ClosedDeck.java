package no.sorthvit.mokk.game.deck;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class ClosedDeck {
    private Stack<Card> cardStack;

    public ClosedDeck(final List<Card> cards) {
        cardStack = new Stack<>();
        Collections.shuffle(cards);
        this.cardStack.addAll(cards);
    }

    public Card drawCard(){
        if (!cardStack.empty()){
            return cardStack.pop();
        }
        return null;
    }

    public boolean hasCards() {
        return !cardStack.empty();
    }

    public void clear() {
        cardStack.clear();
    }

    public int totalCards() {
        return cardStack.size();
    }
}
