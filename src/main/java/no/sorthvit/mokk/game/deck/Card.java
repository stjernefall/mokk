package no.sorthvit.mokk.game.deck;

import no.sorthvit.mokk.gui.CardTextureUtil;
import no.sorthvit.mokk.gui.Component;
import no.sorthvit.mokk.util.CardUtil;
import org.springframework.util.Assert;

public class Card extends Component implements Comparable<Card> {
    private final int value;
    private final int color;
    private boolean showCard;

    public Card(final int value, final int color, final boolean showCard) {
        super(0, 0, "", 0.2f, 0.3f, true, null);
        Assert.isTrue(value > 2 && value < 16, "unvalid value");
        this.value = value;
        this.color = color;
        this.showCard = showCard;
    }

    public int getValue() {
        return value;
    }

    public int getColor() {
        return color;
    }

    public void setShowCard(final boolean showCard) {
        this.showCard = showCard;
    }

    @Override
    public String getImgPath() {
        if (showCard) {
            return CardTextureUtil.getCardImage(this);
        } else {
            return CardTextureUtil.getBacksideCard();
        }
    }

    @Override
    public String toString() {
        return "[" + CardUtil.printCardValue(getValue()) + CardUtil.printCardColor(getColor()) + "]";
    }

    public boolean isEreaseCard() {
        return getValue() == 10 || getValue() == 14;
    }

    @Override
    public int compareTo(final Card o) {
        if (o.getValue() > getValue()) {
            return -1;
        } else if (o.getValue() < getValue()) {
            return 1;
        }
        return 0;
    }

    public boolean isAce() {
        return getValue() == 14;
    }
}