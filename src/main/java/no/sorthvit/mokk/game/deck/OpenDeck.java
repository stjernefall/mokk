package no.sorthvit.mokk.game.deck;

import java.util.*;

public class OpenDeck {
    private Stack<Card> cardStack;

    public OpenDeck() {
        this.cardStack = new Stack<>();
    }

    public Card addCard(final Card card){
        return addCard(Collections.singletonList(card));
    }

    public Card addCard(final List<Card> card){
        if (card.get(0).isEreaseCard() && !isOnlyEreaseCard() && cardStack.size() != 0){
            cardStack.clear();
        } else {
            for (final Card cardToAdd : card) {
                cardStack.push(cardToAdd);
            }
        }
        return topCard();
    }

    public Card topCard(){
        try {
            return cardStack.peek();
        } catch (final EmptyStackException e){
            return null;
        }
    }

    public ArrayList<Card> drawAllCards(){
        final ArrayList<Card> temp = new ArrayList();
        while (!cardStack.empty()){
            temp.add(cardStack.pop());
        }
        return temp;
    }

    @Override
    public String toString() {
        return "OpenDeck{" +
                "cardStack=" + cardStack +
                '}';
    }

    public int totalCards() {
        return cardStack.size();
    }

    public boolean hasCards() {
        return !cardStack.empty();
    }

    public boolean isOnlyEreaseCard() {
        return cardStack.size() == 1 && cardStack.peek().isEreaseCard();
    }

    public void clear() {
        cardStack.clear();
    }

    public List<Card> showAll() {
        return new ArrayList(cardStack);
    }
}
