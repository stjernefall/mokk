package no.sorthvit.mokk.game.player;

public class HumanPlayerImpl extends PlayerBase implements HumanPlayer {

    public HumanPlayerImpl(final int id, final String name, final boolean showCards) {
        super(id, name, true, 0, 0, showCards);
    }
}
