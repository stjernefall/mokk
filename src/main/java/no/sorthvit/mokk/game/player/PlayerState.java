package no.sorthvit.mokk.game.player;

public enum PlayerState {
    START_ROUND,
    WAITING_TO_CHOOSE,
    PLAY_CARD,
    TAKE_A_CHANCE,
    DRAW,
    FINISHED
}
