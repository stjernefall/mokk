package no.sorthvit.mokk.game.player;

import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.gui.CardTextureUtil;
import no.sorthvit.mokk.gui.Drawable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayerBase implements Player, Drawable {
    private final int id;
    private float positionX;
    private float positionY;
    private String name;
    private boolean drawHorizontal;
    private List<Card> cards;
    private boolean showCards;

    protected PlayerBase(final int id, final String name, final boolean drawHorizontal,
                         final float positionX, final float positionY, final boolean showCards) {
        this.id = id;
        this.name = name;
        this.cards = new ArrayList<>();
        this.drawHorizontal = drawHorizontal;
        this.positionX = positionX;
        this.positionY = positionY;
        this.showCards = showCards;
    }

    public int getId() {
        return id;
    }

    public void giveCard(final Card card) {
        if (null != card) {
            this.cards.add(card);
        }
    }

    private void setCards(final List<Card> cards){
        this.cards = cards;
    }

    public String showHand() {
        return this.cards.toString();
    }

    public boolean isFinished() {
        return this.cards.isEmpty();
    }

    public String name() {
        return this.name;
    }


    public void giveCards(final List<Card> cardsToGive) {
        this.cards.addAll(cardsToGive);
    }

    public int totalCards() {
        return cards.size();
    }

    @Override
    public List<Card> getAllCards() {
        Collections.sort(cards);
        return cards;
    }

    @Override
    public void removeCards(final List<Card> cards) {
        this.cards.removeAll(cards);
    }


    @Override
    public boolean drawHorizontal() {
        return drawHorizontal;
    }

    @Override
    public void setDrawHorizontal(final boolean isHorizontal) {
        this.drawHorizontal = isHorizontal;
    }

    @Override
    public float getPositionX() {
        return positionX;
    }

    @Override
    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    @Override
    public float getPositionY() {
        return positionY;
    }

    @Override
    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public String getName() {
        return name;
    }

    @Override
    public void updateCardPosition() {
        final List<Card> cards = new ArrayList<>();
        if (drawHorizontal()) {
            float positionXCalibrated = getPositionX() - (getAllCards().size() / 50f);
            for (int i = 0; i < getAllCards().size(); i++) {
                final Card card = getAllCards().get(i);
                final float space_width_card = 0.05f * i;
                card.setPositionX(positionXCalibrated + space_width_card);
                card.setPositionY(getPositionY());
                card.setDrawHorizontal(true);
                card.setShowCard(showCards);
                cards.add(card);
            }
        } else {
            float positionYCalibrated = getPositionY() - (getAllCards().size() / 50f);
            for (int i = 0; i < getAllCards().size(); i++) {
                final Card card = getAllCards().get(i);
                final float space_width_card = 0.05f * i;
                card.setPositionX(getPositionX());
                card.setPositionY(positionYCalibrated + space_width_card);
                card.setDrawHorizontal(false);
                card.setShowCard(showCards);
                cards.add(card);
            }
        }
        setCards(cards);
    }
}
