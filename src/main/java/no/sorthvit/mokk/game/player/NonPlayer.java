package no.sorthvit.mokk.game.player;

import no.sorthvit.mokk.game.deck.Card;

public class NonPlayer extends PlayerBase {
    public NonPlayer(int id) {
        super(id, "not playing", true, 0, 0, false);
    }

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    public void giveCard(Card card) {

    }

    @Override
    public int totalCards() {
        return 0;
    }
}
