package no.sorthvit.mokk.game.player;

import no.sorthvit.mokk.game.deck.Card;

import java.util.List;

public interface Player {

    int getId();

    void giveCard(final Card card);

    String showHand();

    boolean isFinished();

    String name();

    void giveCards(final List<Card> cardsToGive);

    List<Card> getAllCards();

    int totalCards();

    void removeCards(final List<Card> cards);

    String getName();

    void updateCardPosition();
}
