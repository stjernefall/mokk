package no.sorthvit.mokk.ai;

import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;

import java.util.List;

public interface AiPlayer {
    int getDefensiveChancePercentage();

    int getAggressiveChancePercentage();

    List<Card> tryToUseCard(final GameEngine gameEngine);

    boolean wantToDrawCards(final GameEngine gameEngine);

    boolean decideToTakeAChange(final GameEngine gameEngine);

    AiVersion getAiVersion();
}
