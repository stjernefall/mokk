package no.sorthvit.mokk.ai;

import no.sorthvit.mokk.ai.nn.NnAiUtil;
import no.sorthvit.mokk.ai.nn.NnAiUtilV2;
import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.game.player.PlayerBase;

import java.util.List;

public class NNAIPlayer extends PlayerBase implements AiPlayer {
    private final AiVersion aiVersion;

    public NNAIPlayer(final int id, final String name, final AiVersion aiVersion, final boolean showCards) {
        super(id, name, true, 0, 0, showCards);
        this.aiVersion = aiVersion;
    }

    @Override
    public int getDefensiveChancePercentage() {
        return 0;
    }

    @Override
    public int getAggressiveChancePercentage() {
        return 0;
    }

    @Override
    public List<Card> tryToUseCard(final GameEngine gameEngine) {
        if (aiVersion == AiVersion.V2) {
            return NnAiUtil.decideCardToPlay(super.getAllCards(), gameEngine);
        } else if (aiVersion == AiVersion.V3) {
            return NnAiUtilV2.decideCardToPlay(super.getAllCards(), gameEngine);
        }
        return null;
    }

    @Override
    public boolean wantToDrawCards(final GameEngine gameEngine) {
        if (aiVersion == AiVersion.V2) {
            return NnAiUtil.decideToDraw(super.getAllCards(), gameEngine);
        } else if (aiVersion == AiVersion.V3) {
            return NnAiUtilV2.decideToDraw(super.getAllCards(), gameEngine);
        }
        return false;
    }

    @Override
    public boolean decideToTakeAChange(final GameEngine gameEngine) {
        if (aiVersion == AiVersion.V2) {
            return NnAiUtil.decideToTakeChance(super.getAllCards(), gameEngine);
        } else if (aiVersion == AiVersion.V3) {
            return NnAiUtilV2.decideToTakeChance(super.getAllCards(), gameEngine);
        }
        return false;
    }

    @Override
    public AiVersion getAiVersion() {
        return aiVersion;
    }
}
