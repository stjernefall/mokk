package no.sorthvit.mokk.ai;

public enum AiVersion {
    V2,
    V3;

    AiVersion() {
    }
}
