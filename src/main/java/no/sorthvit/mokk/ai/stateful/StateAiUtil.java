package no.sorthvit.mokk.ai.stateful;

import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.util.CardRulesUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static no.sorthvit.mokk.util.CardRulesUtil.cardCanBeUsed;

public class StateAiUtil {
    private static Random random;

    static {
        random = new Random();
    }

    private StateAiUtil() {
    }

    // todo: lovlig å legge 3 eller 4 like kort //
    public static List<Card> decideCardToPlay(final StateAiPlayerImpl ai,
                                              final List<Card> cardsInHand,
                                              final GameEngine gameEngine) {
        final Card topCard = gameEngine.getOpenDeck().topCard();
        final List<Card> cardsToUse = new ArrayList<>();
        final int playDefense = randomPercentage();
        final int playAggressive = randomPercentage();

        System.out.println("AiPlayer: defensive = " + playDefense + "/" + (ai.getDefensiveChancePercentage() * gameEngine.getOpenDeck().totalCards()));
        System.out.println("AiPlayer: aggressive = " + playAggressive + "/" + (ai.getAggressiveChancePercentage() * gameEngine.getOpenDeck().totalCards()));

        if (cardsInHand.size() == 1 && !gameEngine.getOpenDeck().hasCards()){
            cardsToUse.add(cardsInHand.get(0));
        }
        else if (cardsInHand.size() == 1 && cardsInHand.get(0).getValue() >= gameEngine.getOpenDeck().topCard().getValue()){
            cardsToUse.add(cardsInHand.get(0));
        } else {
            if (null != topCard && playDefense < ai.getDefensiveChancePercentage() * gameEngine.getOpenDeck().totalCards()) {
                System.out.println("Går i defensive modus!");
                cardsToUse.addAll(useEreaseCard(topCard, cardsInHand));
            } else if (null != topCard && playAggressive < ai.getAggressiveChancePercentage() * gameEngine.getOpenDeck().totalCards()) {
                System.out.println("Går i aggressive modus!");
                cardsToUse.addAll(useHighestLegal(topCard, cardsInHand));
            } else {
                System.out.println("Går i vanlig modus!");
                cardsToUse.addAll(useLowestLegal(topCard, cardsInHand));
            }
        }
        return cardsToUse;
    }

    public static boolean decideToTakeAChange(final StateAiPlayerImpl ai, final GameEngine gameEngine) {
        final int playRisky = randomPercentage();
        final int randomHighcard = random.nextInt(15);
        System.out.println("AiPlayer: riskiness = " + playRisky + "/" + (ai.getRiskinessChancePercentage() * gameEngine.getOpenDeck().totalCards()));
        if (playRisky < ai.getRiskinessChancePercentage() * (gameEngine.getOpenDeck().totalCards() * 0.5) &&
                (gameEngine.getOpenDeck().topCard() == null ? 0 : gameEngine.getOpenDeck().topCard().getValue()) <= randomHighcard) {
            System.out.println("Går i ta en sjanse!");
            return true;
        }
        return false;
    }

    public static boolean wantToDrawCards(final StateAiPlayerImpl ai, final GameEngine gameEngine) {
        final int playRisky = randomPercentage();
        System.out.println("AiPlayer: riskiness = " + playRisky + "/" + (ai.getRiskinessChancePercentage() * gameEngine.getOpenDeck().totalCards()));
        final List<Card> cardsOnTable = gameEngine.getOpenDeck().showAll();
        if (ai.usesDrawTactic() && findAverageSum(cardsOnTable) > 11 && ai.totalCards() < 6 &&
                ai.totalCards() > 2 && playRisky < ai.getRiskinessChancePercentage() * (gameEngine.getOpenDeck().totalCards() * 0.5)) {
            System.out.println("Går i samle bra kort modus!");
            return true;
        }
        return false;
    }

    public static int getRandomAttribute() {
        return random.nextInt(20 - 1 + 1) + 1;
    }

    private static int findAverageSum(final List<Card> cardsOnTable) {
        int averageSum = 0;
        for (final Card card : cardsOnTable) {
            averageSum = averageSum + card.getValue();
        }
        return averageSum != 0 ? (averageSum / cardsOnTable.size()) : 0;
    }

    private static List<Card> useEreaseCard(final Card cardOnTable, final List<Card> cards) {
        for (final Card card : cards) {
            if (card.isEreaseCard() && CardRulesUtil.cardCanBeUsed(cardOnTable, card)) {
                return Collections.singletonList(card);
            }
        }
        return useLowestLegal(cardOnTable, cards);
    }

    private static List<Card> useHighestLegal(final Card cardOnTable, final List<Card> cards) {
        Card cardToUse = null;
        for (final Card card : cards) {
            if (cardToUse == null) {
                if (card.getValue() != 10 && card.getValue() != 14) {
                    cardToUse = card;
                }
            } else if (card.getValue() > cardToUse.getValue() && card.getValue() != 10 && card.getValue() != 14) {
                cardToUse = card;
            }
        }

        if (cardCanBeUsed(cardOnTable, cardToUse)) {
            return Collections.singletonList(cardToUse);
        }
        return useLowestLegal(cardOnTable, cards);
    }

    public static List<Card> useLowestLegal(final Card cardOnTable, final List<Card> cards) {
        Card cardToUse = cards.get(0);
        for (final Card cardToEvaluate : cards) {
            if (cardOnTable == null) {
                if (cardToUse.getValue() > cardToEvaluate.getValue()) {
                    cardToUse = cardToEvaluate;
                }
            } else {
                if (cardOnTable.getValue() <= cardToEvaluate.getValue()) {
                    if (cardToUse.getValue() < cardOnTable.getValue()) {
                        cardToUse = cardToEvaluate;
                    } else if (cardToEvaluate.getValue() <= cardToUse.getValue() &&
                            cardToEvaluate.getValue() >= cardOnTable.getValue()) {
                        cardToUse = cardToEvaluate;
                    }
                }
            }
        }

        if (cardCanBeUsed(cardOnTable, cardToUse)) {
            return Collections.singletonList(cardToUse);
        }
        return new ArrayList<>();
    }

    private static int randomPercentage() {
        return random.nextInt(100) + 1;
    }

    public static boolean getRandomBoolean() {
        return random.nextBoolean();
    }
}
