package no.sorthvit.mokk.ai.stateful;

import no.sorthvit.mokk.ai.AiVersion;
import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.ai.AiPlayer;
import no.sorthvit.mokk.game.player.PlayerBase;

import java.util.List;

public class StateAiPlayerImpl extends PlayerBase implements AiPlayer {
    private boolean usesDrawTactic;
    private int defensiveChancePercentage;
    private int aggressiveChancePercentage;
    private int riskinessChancePercentage;

    public StateAiPlayerImpl(final int id,
                             final String name,
                             final int aggressiveChancePercentage,
                             final int defensiveChancePercentage,
                             final int riskinessChancePercentage,
                             final boolean usesDrawTactic,
                             final boolean showCards) {
        super(id, name, true, 0, 0, showCards);
        this.usesDrawTactic = usesDrawTactic;
        this.aggressiveChancePercentage = aggressiveChancePercentage;
        this.defensiveChancePercentage = defensiveChancePercentage;
        this.riskinessChancePercentage = riskinessChancePercentage;
    }

    public boolean usesDrawTactic() {
        return usesDrawTactic;
    }

    public void setUsesDrawTactic(boolean usesDrawTactic) {
        this.usesDrawTactic = usesDrawTactic;
    }

    public int getDefensiveChancePercentage() {
        return defensiveChancePercentage;
    }

    public void setDefensiveChancePercentage(int defensiveChancePercentage) {
        this.defensiveChancePercentage = defensiveChancePercentage;
    }

    public int getAggressiveChancePercentage() {
        return aggressiveChancePercentage;
    }

    public int getRiskinessChancePercentage() {
        return riskinessChancePercentage;
    }

    public void setRiskinessChancePercentage(int riskinessChancePercentage) {
        this.riskinessChancePercentage = riskinessChancePercentage;
    }

    @Override
    public List<Card> tryToUseCard(final GameEngine gameEngine) {
        return StateAiUtil.decideCardToPlay(this, super.getAllCards(), gameEngine);
    }

    public void setAggressiveChancePercentage(int aggressiveChancePercentage) {
        this.aggressiveChancePercentage = aggressiveChancePercentage;
    }

    @Override
    public boolean wantToDrawCards(final GameEngine gameEngine) {
        return StateAiUtil.wantToDrawCards(this, gameEngine);
    }

    @Override
    public boolean decideToTakeAChange(final GameEngine gameEngine) {
        return StateAiUtil.decideToTakeAChange(this, gameEngine);
    }

    @Override
    public AiVersion getAiVersion() {
        return null;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StateAiPlayerImpl player = (StateAiPlayerImpl) o;

        if (defensiveChancePercentage != player.defensiveChancePercentage) return false;
        if (aggressiveChancePercentage != player.aggressiveChancePercentage) return false;
        if (super.getName() != null ? !super.getName().equals(player.getName()) : player.getName() != null)
            return false;
        return super.getAllCards() != null ? super.getAllCards().equals(player.getAllCards()) : player.getAllCards() == null;
    }

    @Override
    public int hashCode() {
        int result = super.getName() != null ? super.getName().hashCode() : 0;
        result = 31 * result + (super.getAllCards() != null ? super.getAllCards().hashCode() : 0);
        result = 31 * result + defensiveChancePercentage;
        result = 31 * result + aggressiveChancePercentage;
        return result;
    }
}
