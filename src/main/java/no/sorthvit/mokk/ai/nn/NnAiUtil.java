package no.sorthvit.mokk.ai.nn;

import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.game.player.PlayerState;
import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.neural.data.basic.BasicNeuralData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.persist.EncogDirectoryPersistence;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class NnAiUtil {
    private static BasicNetwork network;

    public static void init() throws IOException {
        AIDataConverter.initWriter();
        network = (BasicNetwork) EncogDirectoryPersistence.loadObject(new ClassPathResource("weightv2.csv").getInputStream());
    }

    private static BasicNeuralData createInput(final List<Card> playersHand, final GameEngine gameEngine) {
        final double[] input = Arrays.stream(AIDataConverter.createDataForPlaying(playersHand, gameEngine))
                .mapToDouble(Double::parseDouble)
                .toArray();
        return new BasicNeuralData(input);
    }

    public static boolean decideToDraw(final List<Card> cardsInHand, final GameEngine gameEngine) {
        final BasicNeuralData dataSet = createInput(cardsInHand, gameEngine);
        final MLData dataOutput = network.compute(dataSet);
        System.out.println(Arrays.toString(dataOutput.getData()));
        return checkActionToTake(dataOutput.getData(), PlayerState.DRAW);
    }

    public static boolean decideToTakeChance(final List<Card> cardsInHand, final GameEngine gameEngine) {
        final BasicNeuralData dataSet = createInput(cardsInHand, gameEngine);
        final MLData dataOutput = network.compute(dataSet);
        System.out.println(Arrays.toString(dataOutput.getData()));
        return checkActionToTake(dataOutput.getData(), PlayerState.TAKE_A_CHANCE);
    }

    public static List<Card> decideCardToPlay(final List<Card> cardsInHand,
                                              final GameEngine gameEngine) {

        if (cardsInHand.size() == 1 && !gameEngine.getOpenDeck().hasCards()) {
            final List<Card> cards = new ArrayList<>();
            cards.add(cardsInHand.get(0));
            return cards;
        } else if (cardsInHand.size() == 1 && cardsInHand.get(0).getValue() >= gameEngine.getOpenDeck().topCard().getValue()) {
            final List<Card> cards = new ArrayList<>();
            cards.add(cardsInHand.get(0));
            return cards;
        }

        final BasicNeuralData dataSet = createInput(cardsInHand, gameEngine);
        final MLData suggestedPlay = network.compute(dataSet);
        System.out.println(Arrays.toString(suggestedPlay.getData()));

        int positionFirst = 0;
        double valueFirst = suggestedPlay.getData()[0];
        int positionSecond = 0;
        double valueSecond = suggestedPlay.getData()[0];
        int positionThird = 0;
        double valueThird = suggestedPlay.getData()[0];
        for (int i = 0; i < suggestedPlay.getData().length - 2; i++) {
            if (suggestedPlay.getData()[i] > valueFirst) {
                positionThird = positionSecond;
                valueThird = valueSecond;
                positionSecond = positionFirst;
                valueSecond = valueFirst;
                valueFirst = suggestedPlay.getData()[i];
                positionFirst = i;
            }
        }

        System.out.println("AI førstevalg: " + getCardsBasedOnPostion(positionFirst) + " med en " + valueFirst + " sikkerhet");
        System.out.println("AI andrevalg: " + getCardsBasedOnPostion(positionSecond) + " med en " + valueSecond + " sikkerhet");
        System.out.println("AI tredjevalg: " + getCardsBasedOnPostion(positionThird) + " med en " + valueThird + " sikkerhet");

        final ArrayList<Card> card = new ArrayList<>();
        int firstCardsHandPosition = -1;
        int secondCardsHandPosition = -1;
        int thirdCardsHandPosition = -1;
        for (int i = 0; i < cardsInHand.size(); i++) {
            if (cardsInHand.get(i).getValue() == Integer.parseInt(Objects.requireNonNull(getCardsBasedOnPostion(positionFirst)))) {
                firstCardsHandPosition = i;
            }
            if (cardsInHand.get(i).getValue() == Integer.parseInt(Objects.requireNonNull(getCardsBasedOnPostion(positionSecond)))) {
                secondCardsHandPosition = i;
            }
            if (cardsInHand.get(i).getValue() == Integer.parseInt(Objects.requireNonNull(getCardsBasedOnPostion(positionThird)))) {
                thirdCardsHandPosition = i;
            }
        }

        if (firstCardsHandPosition != -1) {
            card.add(cardsInHand.get(firstCardsHandPosition));
        } else if (secondCardsHandPosition != -1) {
            card.add(cardsInHand.get(secondCardsHandPosition));
        } else if (thirdCardsHandPosition != -1) {
            card.add(cardsInHand.get(thirdCardsHandPosition));
        }
        return card;
    }

    // three - four - five - six - seven - eight - nine - ten - jack - queen - king - ace - to
    private static String getCardsBasedOnPostion(int position) {
        if (position == 0) {
            return "3";
        } else if (position == 1) {
            return "4";
        } else if (position == 2) {
            return "5";
        } else if (position == 3) {
            return "6";
        } else if (position == 4) {
            return "7";
        } else if (position == 5) {
            return "8";
        } else if (position == 6) {
            return "9";
        } else if (position == 7) {
            return "10";
        } else if (position == 8) {
            return "11";
        } else if (position == 9) {
            return "12";
        } else if (position == 10) {
            return "13";
        } else if (position == 11) {
            return "14";
        } else if (position == 12) {
            return "15";
        } else if (position == 13) {
            return "TAKE_A_CHANCE";
        } else if (position == 14) {
            return "DRAW";
        }
        return null;
    }

    private static boolean checkActionToTake(final double[] data, final PlayerState checkAction) {
        double playCardValue = 0;
        for (int i = 0; i < data.length - 2; i++) {
            if (data[i] > playCardValue) {
                playCardValue = data[i];
            }
        }

        if (data[13] > data[14] && data[13] > playCardValue && checkAction == PlayerState.TAKE_A_CHANCE) {
            return true;
        } else if (data[14] > data[13] && data[14] > playCardValue && checkAction == PlayerState.DRAW) {
            return true;
        } else return checkAction == PlayerState.PLAY_CARD;
    }

    public static void terminate() {
        AIDataConverter.closeWriter();
        Encog.getInstance().shutdown();
    }
}
