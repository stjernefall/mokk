package no.sorthvit.mokk.ai.nn;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class AIDataConverterV2 {
    static private CSVWriter featureWriter = null;
    static private CSVWriter labelWriter = null;

    private AIDataConverterV2() {
    }

    public static void initWriter() {
        try {
            final Path pathfeature = Paths.get(ClassLoader.getSystemResource("feature.csv").toURI());
            final Path pathLabel = Paths.get(ClassLoader.getSystemResource("label.csv").toURI());
            featureWriter = new CSVWriter(new FileWriter(pathfeature.toString(), true));
            labelWriter = new CSVWriter(new FileWriter(pathLabel.toString(), true));
        } catch (final Exception e) {
            System.out.println(e);
        }
    }

    public static void writeData(final List<String[]> dataToWrite) {
        for (final String[] data : dataToWrite) {
            writeOneLine(data);
        }
    }

    public static void closeWriter() {
        try {
            featureWriter.close();
            labelWriter.close();
        } catch (final IOException e) {
            System.out.println(e);
        }
    }

    // Split data //
    private static void writeOneLine(final String[] array) {
        final String[] feature = new String[32];
        final String[] label = new String[15];

        System.arraycopy(array, 0, feature, 0, feature.length);
        System.arraycopy(array, feature.length, label, 0, label.length);

        featureWriter.writeNext(feature, false);
        labelWriter.writeNext(label, false);
    }
}
