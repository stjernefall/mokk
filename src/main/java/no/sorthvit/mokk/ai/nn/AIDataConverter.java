package no.sorthvit.mokk.ai.nn;

import com.opencsv.CSVWriter;
import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.game.player.Player;
import no.sorthvit.mokk.game.player.PlayerState;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class AIDataConverter {
    static private CSVWriter writer = null;

    private AIDataConverter() {
    }

    public static void initWriter() {
        try {
            final Path path = Paths.get(ClassLoader.getSystemResource("training.csv").toURI());
            writer = new CSVWriter(new FileWriter(path.toString(), true));
        } catch (final Exception e) {
            System.out.println(e);
        }
    }

    public static String[] createDataForTraining(final List<Card> playersHand, final GameEngine gameEngine,
                                                 final Card cardUsed, final PlayerState playerState) {
        return new String[]{
                // input //
                contains(playersHand, 3), // 1
                contains(playersHand, 4),
                contains(playersHand, 5),
                contains(playersHand, 6),
                contains(playersHand, 7),
                contains(playersHand, 8),
                contains(playersHand, 9),
                contains(playersHand, 10),
                contains(playersHand, 11),
                contains(playersHand, 12),
                contains(playersHand, 13),
                contains(playersHand, 14),
                contains(playersHand, 15),
                isEmpty(gameEngine.getTopCard()),// 13
                isTheSame(gameEngine.getTopCard(), 3),
                isTheSame(gameEngine.getTopCard(), 4),
                isTheSame(gameEngine.getTopCard(), 5),
                isTheSame(gameEngine.getTopCard(), 6),
                isTheSame(gameEngine.getTopCard(), 7),
                isTheSame(gameEngine.getTopCard(), 8),
                isTheSame(gameEngine.getTopCard(), 9),
                isTheSame(gameEngine.getTopCard(), 10),
                isTheSame(gameEngine.getTopCard(), 11),
                isTheSame(gameEngine.getTopCard(), 12),
                isTheSame(gameEngine.getTopCard(), 13),
                isTheSame(gameEngine.getTopCard(), 14),
                isTheSame(gameEngine.getTopCard(), 15), // 27
                isFinished(gameEngine.getAllPlayers().get(0)),
                isFinished(gameEngine.getAllPlayers().get(1)),
                isFinished(gameEngine.getAllPlayers().size() > 2 ? gameEngine.getAllPlayers().get(2) : null),
                isFinished(gameEngine.getAllPlayers().size() > 3 ? gameEngine.getAllPlayers().get(3) : null),
                String.valueOf(gameEngine.getOpenDeck().totalCards()), // 32
                // expected //
                isTheSame(cardUsed, 3),
                isTheSame(cardUsed, 4),
                isTheSame(cardUsed, 5),
                isTheSame(cardUsed, 6),
                isTheSame(cardUsed, 7),
                isTheSame(cardUsed, 8),
                isTheSame(cardUsed, 9),
                isTheSame(cardUsed, 10),
                isTheSame(cardUsed, 11),
                isTheSame(cardUsed, 12),
                isTheSame(cardUsed, 13),
                isTheSame(cardUsed, 14),
                isTheSame(cardUsed, 15), //45 (husk - 1)
                isPlayerState(playerState, PlayerState.TAKE_A_CHANCE),
                isPlayerState(playerState, PlayerState.DRAW)
        };
    }

    public static String[] createDataForPlaying(final List<Card> playersHand, final GameEngine gameEngine) {
        return new String[]{
                contains(playersHand, 3), // 0
                contains(playersHand, 4),
                contains(playersHand, 5),
                contains(playersHand, 6),
                contains(playersHand, 7),
                contains(playersHand, 8),
                contains(playersHand, 9),
                contains(playersHand, 10),
                contains(playersHand, 11),
                contains(playersHand, 12),
                contains(playersHand, 13),
                contains(playersHand, 14),
                contains(playersHand, 15),
                isEmpty(gameEngine.getTopCard()),// 13
                isTheSame(gameEngine.getTopCard(), 3),
                isTheSame(gameEngine.getTopCard(), 4),
                isTheSame(gameEngine.getTopCard(), 5),
                isTheSame(gameEngine.getTopCard(), 6),
                isTheSame(gameEngine.getTopCard(), 7),
                isTheSame(gameEngine.getTopCard(), 8),
                isTheSame(gameEngine.getTopCard(), 9),
                isTheSame(gameEngine.getTopCard(), 10),
                isTheSame(gameEngine.getTopCard(), 11),
                isTheSame(gameEngine.getTopCard(), 12),
                isTheSame(gameEngine.getTopCard(), 13),
                isTheSame(gameEngine.getTopCard(), 14),
                isTheSame(gameEngine.getTopCard(), 15), // 26
                isFinished(gameEngine.getAllPlayers().get(0)),
                isFinished(gameEngine.getAllPlayers().get(1)),
                isFinished(gameEngine.getAllPlayers().size() > 2 ? gameEngine.getAllPlayers().get(2) : null),
                isFinished(gameEngine.getAllPlayers().size() > 3 ? gameEngine.getAllPlayers().get(3) : null),
                String.valueOf(gameEngine.getOpenDeck().totalCards()) // 31
        };
    }

    public static void writeData(final List<String[]> dataToWrite) {
        for (final String[] data : dataToWrite) {
            writeOneLine(data);
        }
    }

    private static String isPlayerState(final PlayerState currentState, final PlayerState compareState) {
        if (currentState.equals(compareState)) {
            return "1";
        }
        return "0";
    }

    private static String isFinished(final Player player) {
        if (null != player && player.isFinished()) {
            return "1";
        }
        return "0";
    }

    public static void closeWriter() {
        try {
            writer.close();
        } catch (final IOException e) {
            System.out.println(e);
        }
    }

    private static void writeOneLine(final String[] array) {
        writer.writeNext(array, false);
    }

    private static String isEmpty(final Card card) {
        if (null == card) {
            return "1";
        }
        return "0";
    }

    private static String isTheSame(final Card card, final int value) {
        if (null != card && card.getValue() == value) {
            return "1";
        }
        return "0";
    }

    private static String contains(final List<Card> cards, final int value) {
        for (final Card card : cards) {
            if (isTheSame(card, value).equals("1")) {
                return "1";
            }
        }
        return "0";
    }
}
