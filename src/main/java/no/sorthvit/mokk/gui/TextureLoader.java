package no.sorthvit.mokk.gui;

import de.matthiasmann.twl.utils.PNGDecoder;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30C.glGenerateMipmap;

public class TextureLoader {

    private static final Map<String, Texture> textureMap = new HashMap<>();

    public static Map<String, Texture> getTextureMap() {
        return textureMap;
    }

    public static Texture loadTexture(final String fileName) {

        try {
            if (textureMap.containsKey(fileName)) {
                final Texture texture = textureMap.get(fileName);
                texureInit(texture);
                return texture;
            } else {
                //load png file
                //final PNGDecoder decoder = new PNGDecoder(TextureLoader.class.getResourceAsStream(fileName));
                final PNGDecoder decoder = new PNGDecoder(new ClassPathResource(fileName).getInputStream());

                //create a byte buffer big enough to store RGBA values
                final ByteBuffer buffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());

                //decode
                decoder.decode(buffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);

                //flip the buffer so its ready to read
                buffer.flip();

                //create a texture
                int id = glGenTextures();

                final Texture texture = new Texture(id, decoder.getWidth(),  decoder.getHeight(), buffer);
                textureMap.put(fileName, texture);
                texureInit(texture);
                return texture;
            }
        } catch (final IOException e) {
            System.out.println(e);
        }
        return null;
    }

    private static void texureInit(final Texture texture){
        //bind the texture
        glBindTexture(GL_TEXTURE_2D, texture.getId());

        //tell opengl how to unpack bytes
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        //set the texture parameters, can be GL_LINEAR or GL_NEAREST
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //upload texture
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture.getWidth(), texture.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.getBuffer());

        // Generate Mip Map
        glGenerateMipmap(GL_TEXTURE_2D);
    }
}
