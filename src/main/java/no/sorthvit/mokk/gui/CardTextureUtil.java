package no.sorthvit.mokk.gui;

import no.sorthvit.mokk.game.deck.Card;

public class CardTextureUtil {
    private CardTextureUtil(){

    }

    private static String cardValue(final Integer value){
        switch (value){
            case 11:
                return "jack";
            case 12:
                return "queen";
            case 13:
                return "king";
            case 14:
                return "ace";
            case 15:
                return "2";
            default:
                return value.toString();
        }
    }

    private static String cardColor(final Integer value){
        switch (value){
            case 0:
                return "_of_spades"; // Spade
            case 1:
                return "_of_diamonds"; // diamonds
            case 2:
                return "_of_clubs"; // clubs
            case 3:
                return "_of_hearts"; // hearts
            default:
                return "";
        }
    }

    public static String getCardImage(final Card card) {
        return "cards/" + cardValue(card.getValue()) + cardColor(card.getColor()) + ".png";
    }

    public static String getBacksideCard() {
        return "cards/backside.png";
    }
}
