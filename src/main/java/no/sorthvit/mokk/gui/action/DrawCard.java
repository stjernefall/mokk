package no.sorthvit.mokk.gui.action;

import no.sorthvit.mokk.gui.Component;
import no.sorthvit.mokk.game.player.PlayerState;

public class DrawCard extends Component {

    public DrawCard(float positionX, float positionY, float width, float height) {
        super(positionX, positionY, "actions/card-draw.png", width, height,true, PlayerState.DRAW);
    }
}
