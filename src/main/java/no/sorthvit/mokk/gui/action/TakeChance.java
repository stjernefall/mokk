package no.sorthvit.mokk.gui.action;

import no.sorthvit.mokk.gui.Component;
import no.sorthvit.mokk.game.player.PlayerState;

public class TakeChance extends Component {

    public TakeChance(float positionX, float positionY, float width, float height) {
        super(positionX, positionY, "actions/uncertainty.png", width, height,true, PlayerState.TAKE_A_CHANCE);
    }
}
