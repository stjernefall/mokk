package no.sorthvit.mokk.gui;

import java.nio.ByteBuffer;

public class Texture{
    private int id;
    private int width;
    private int height;
    private ByteBuffer buffer;

    public Texture(int id, int width, int height, ByteBuffer buffer){
        this.id = id;
        this.width = width;
        this.height = height;
        this.buffer = buffer;
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }
}