package no.sorthvit.mokk.gui;

import no.sorthvit.mokk.game.deck.Card;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.List;
import java.util.Map;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class GraphicEngine {

    private long window;
    private int windowWidth;
    private int windowHeight;

    public GraphicEngine(final int windowWidth, final int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
    }

    public void init() {
        initGraphic();
    }

    public int getWindowWidth() {
        return windowWidth;
    }

    public int getWindowHeight() {
        return windowHeight;
    }

    private void initGraphic() {
        GLFWErrorCallback.createPrint(System.err).set();
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        window = glfwCreateWindow(windowWidth, windowHeight, "Møkk", NULL, NULL);
        if (window == NULL) {
            throw new RuntimeException("Failed to create the GLFW window");
        }

        // Get the thread stack and push a new frame
        try (final MemoryStack stack = stackPush()) {
            final IntBuffer pWidth = stack.mallocInt(1); // int*
            final IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            final GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            if (vidmode == null)
                throw new RuntimeException("Failed to create the GLFW window");

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        }

        glfwMakeContextCurrent(window);
        glfwShowWindow(window);
        glfwSwapInterval(5);
        GL.createCapabilities();
    }

    public void preDraw() {
        // Camera //
        GL11.glOrtho(0, windowWidth, windowHeight, 0, 10, -10);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glDisable(GL11.GL_LIGHTING);

        // Clear to redraw //
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
        glPushMatrix();
    }

    public void drawPlayerCards(final List<Card> cardList) {
        for (final Component card : cardList) {
            if (card.drawHorizontal()) {
                drawQuadHorizontal(card);
            } else {
                drawQuadVertical(card);
            }
        }
    }

    public void drawActions(final List<Component> componentList) {
        for (final Component component : componentList) {
            drawQuadHorizontal(component);
        }
    }

    public void postDraw() {
        glPopMatrix();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    public void drawBackground() {
        glBindTexture(1, TextureLoader.loadTexture("bg.png").getId());
        drawQuadFullScreen();
    }

    public void drawTableCards(final Card topCard, final boolean hasDrawableCards) {
        final float distanceBetweenStacks = 0.25f;
        final float positionX = -0.05f;
        final float positionY = 0.0f;

        if (null != topCard) {
            final String image = CardTextureUtil.getCardImage(topCard);
            glBindTexture(1, TextureLoader.loadTexture(image).getId());
            drawCardQuadHorizontal(positionX, positionY);
        }

        if (hasDrawableCards) {
            glBindTexture(1, TextureLoader.loadTexture(CardTextureUtil.getBacksideCard()).getId());
            drawCardQuadHorizontal(positionX + distanceBetweenStacks, positionY);
        }
    }

    private void drawQuadVertical(final Component component) {
        glBindTexture(1, TextureLoader.loadTexture(component.getImgPath()).getId());

        glBegin(GL_QUADS);
        {
            // Oppe venstre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 0.0f);
            glVertex2f(component.getPositionX(), component.getPositionY() - component.getWidth());

            // Nede venstre  X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 0.0f);
            glVertex2f(component.getPositionX(), component.getPositionY());

            // Nede Høyre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 1.0f);
            glVertex2f(component.getPositionX() + component.getHeight(), component.getPositionY());

            // Oppe Høyre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 1.0f);
            glVertex2f(component.getPositionX() + component.getHeight(), component.getPositionY() - component.getWidth());

        }
        glEnd();
    }

    private void drawQuadHorizontal(final Component component) {
        glBindTexture(1, TextureLoader.loadTexture(component.getImgPath()).getId());

        glBegin(GL_QUADS);
        {
            // Oppe venstre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 0.0f);
            glVertex2f(component.getPositionX(), component.getPositionY());

            // Nede venstre  X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 1.0f);
            glVertex2f(component.getPositionX(), component.getPositionY() - component.getHeight());

            // Nede Høyre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 1.0f);
            glVertex2f(component.getPositionX() + component.getWidth(), component.getPositionY() - component.getHeight());

            // Oppe Høyre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 0.0f);
            glVertex2f(component.getPositionX() + component.getWidth(), component.getPositionY());

        }
        glEnd();
    }

    private void drawCardQuadHorizontal(final float positionX, final float positionY) {
        final float space_width = -0.2f;
        final float space_height = -0.3f;

        glBegin(GL_QUADS);
        {
            // Nede høyre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 0.0f);
            glVertex2f(positionX + space_width, positionY - space_height);

            // Nede venstre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 0.0f);
            glVertex2f(positionX, positionY - space_height);

            // Oppe Høyre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 1.0f);
            glVertex2f(positionX, positionY);

            // Oppe Venstre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 1.0f);
            glVertex2f(positionX + space_width, positionY);

        }
        glEnd();
    }

    private void drawQuadFullScreen() {
        glBegin(GL_QUADS);
        {
            // Nede høyre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 0.0f);
            glVertex2f(-1.0f, -1.0f);

            // Nede venstre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 0.0f);
            glVertex2f(1.0f, -1.0f);

            // Oppe Høyre X / Y (-1 -> +1)
            glTexCoord2f(1.0f, 1.0f);
            glVertex2f(1.0f, 1.0f);

            // Oppe Venstre X / Y (-1 -> +1)
            glTexCoord2f(0.0f, 1.0f);
            glVertex2f(-1.0f, 1.0f);
        }
        glEnd();
    }

    private void cleanTextures() {
        for (final Map.Entry<String, Texture> textureEntry : TextureLoader.getTextureMap().entrySet()) {
            glDeleteTextures(textureEntry.getValue().getId());
        }
    }

    public void terminate() {
        System.out.println("Terminate");
        cleanTextures();
        glDisable(GL_TEXTURE_2D);
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    public long getWindow() {
        return window;
    }

    public void drawHighlightedCard(final Card highlightedCard) {
        if (null != highlightedCard){
            final Card card = new Card(highlightedCard.getValue(), highlightedCard.getColor(), true);
            card.setWidth(highlightedCard.getWidth() + 0.03f);
            card.setHeight(highlightedCard.getHeight() + 0.03f);
            if (highlightedCard.drawHorizontal()) {
                card.setPositionY(highlightedCard.getPositionY() + 0.015f);
                card.setPositionX(highlightedCard.getPositionX() - 0.015f);
                drawQuadHorizontal(card);
            } else {
                card.setPositionY(highlightedCard.getPositionY() + 0.015f);
                card.setPositionX(highlightedCard.getPositionX() - 0.015f);
                drawQuadVertical(card);
            }
        }
    }

    public void drawDesign() {
        glBindTexture(1, TextureLoader.loadTexture("bgdesign.png").getId());
        drawQuadFullScreen();
    }
}
