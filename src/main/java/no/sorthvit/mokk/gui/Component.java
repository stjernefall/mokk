package no.sorthvit.mokk.gui;

import no.sorthvit.mokk.game.player.PlayerState;

public class Component implements Drawable{
    private float positionX;
    private float positionY;
    private String imgPath;
    private float width;
    private float height;
    private boolean drawHorizontal;
    private PlayerState playerState;

    protected Component(final float positionX, final float positionY, final String imgPath, final float width, final float height,
                        final boolean drawHorizontal, final PlayerState playerState) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.imgPath = imgPath;
        this.width = width;
        this.height = height;
        this.drawHorizontal = drawHorizontal;
        this.playerState = playerState;
    }

    @Override
    public boolean drawHorizontal() {
        return drawHorizontal;
    }

    @Override
    public void setDrawHorizontal(boolean drawHorizontal) {
        this.drawHorizontal = drawHorizontal;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }
}
