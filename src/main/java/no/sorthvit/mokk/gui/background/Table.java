package no.sorthvit.mokk.gui.background;

import no.sorthvit.mokk.gui.Component;

public class Table extends Component {

    protected Table(float positionX, float positionY, float width, float height) {
        super(positionX, positionY, "bg.png", width, height, true,null);
    }
}
