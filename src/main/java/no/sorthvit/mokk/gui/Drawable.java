package no.sorthvit.mokk.gui;

public interface Drawable {
    boolean drawHorizontal();

    void setDrawHorizontal(boolean isHorizontal);

    float getPositionX();

    void setPositionX(float positionX);

    float getPositionY();

    void setPositionY(float positionY);
}
