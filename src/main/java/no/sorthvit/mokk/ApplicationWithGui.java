package no.sorthvit.mokk;

import no.sorthvit.mokk.ai.AiPlayer;
import no.sorthvit.mokk.ai.AiVersion;
import no.sorthvit.mokk.ai.NNAIPlayer;
import no.sorthvit.mokk.ai.nn.NnAiUtil;
import no.sorthvit.mokk.ai.nn.NnAiUtilV2;
import no.sorthvit.mokk.cursor.Cursor;
import no.sorthvit.mokk.game.GameEngine;
import no.sorthvit.mokk.game.deck.Card;
import no.sorthvit.mokk.game.player.HumanPlayerImpl;
import no.sorthvit.mokk.game.player.Player;
import no.sorthvit.mokk.game.player.PlayerBase;
import no.sorthvit.mokk.game.player.PlayerState;
import no.sorthvit.mokk.game.stats.Stats;
import no.sorthvit.mokk.gui.Component;
import no.sorthvit.mokk.gui.GraphicEngine;
import no.sorthvit.mokk.gui.action.DrawCard;
import no.sorthvit.mokk.gui.action.TakeChance;
import no.sorthvit.mokk.util.CardRulesUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import java.io.IOException;
import java.nio.DoubleBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static no.sorthvit.mokk.cursor.CusorUtil.pointInsideSquare;
import static no.sorthvit.mokk.game.player.PlayerState.*;
import static org.lwjgl.glfw.GLFW.*;

public class ApplicationWithGui {
    private GraphicEngine graphicEngine;
    private GameEngine gameEngine;

    private static boolean graphicOn = false;
    private static final int delayMS = 0;
    private volatile boolean gameRunning = true;
    private static final int TOTAL_GAMES = 10;
    private static final boolean useCheat = false;

    // Player state //
    private List<PlayerBase> players;
    private PlayerState playerState = null;
    private Card cardToUse = null;

    // Animation //
    private Card highlightedCard = null;

    // GUI //
    private List<Component> actionList = null;

    // game stats //
    private static int placement = 0;
    private int currentGame = 0;
    private static List<Stats> stats = new ArrayList<>();
    private static Map<Integer, Integer> playerStatsEndPostions;
    private static Map<Integer, List<String[]>> playerMoves;

    public static void main(final String[] args) throws Exception {
        if (System.getProperty("org.lwjgl.librarypath") == null) {
            Path path = Paths.get("libs");
            String librarypath = path.toAbsolutePath().toString();
            System.setProperty("org.lwjgl.librarypath", librarypath);
            System.out.println(librarypath);
        }

        final ApplicationWithGui game = new ApplicationWithGui();
        if (graphicOn) {
            game.initGraphics();
        }
        game.initGame();
        game.initSound();
        while (game.getCurrentGame() < TOTAL_GAMES) {
            game.resetGame();
            game.gameLoop();
            game.increaseCurrentGame();

            // Write down winners move //
//            writeWinnersMove();
        }
        game.displayStats();
        game.terminateGame();
    }

    private void increaseCurrentGame() {
        this.currentGame = currentGame + 1;
    }

    private int getCurrentGame() {
        return currentGame;
    }

    private void initGraphics() {
        // Graphics //
        graphicEngine = new GraphicEngine(800, 800);
        graphicEngine.init();

        // Compoents //
        actionList = new ArrayList<>();
        actionList.add(new DrawCard(0.7f, -0.7f, 0.15f, 0.15f));
        actionList.add(new TakeChance(0.5f, -0.7f, 0.15f, 0.15f));
    }

    private void initSound() {

    }

    private void initGame() throws Exception {
        // Ai //
        NnAiUtil.init();
        NnAiUtilV2.init();
        resetGame();
    }

    private static void writeWinnersMove() {
        Integer winnerId = null;
//        Integer secondplaceId = null;
        for (Map.Entry<Integer, Integer> entry : playerStatsEndPostions.entrySet()) {
            if (entry.getValue() == 0) {
                winnerId = entry.getKey();
            }
//            else if(entry.getValue() == 1){
//                secondplaceId = entry.getKey();
//            }
        }

//        AIDataConverterV2.writeData(playerMoves.get(winnerId));
//        AIDataConverterV2.writeData(playerMoves.get(secondplaceId));
    }

    private void resetGame() throws Exception {
        // Players //
        placement = 0;
        playerStatsEndPostions = new HashMap<>();
        playerMoves = new HashMap<>();
        playerState = START_ROUND;
//        players.add(new HumanPlayerImpl(1, "Kenneth", true));
        players = new ArrayList<>();
        players.add(new NNAIPlayer(1, "A - V3", AiVersion.V3, useCheat));
        players.add(new NNAIPlayer(2, "B - V3", AiVersion.V3, useCheat));

        // Game engine //
        gameRunning = true;
        gameEngine = new GameEngine(players);
        gameEngine.initNewGame();
    }

    private void gameLoop() {
        while (gameRunning) {
//            if (!gameEngine.isGameFinished()) {
            if (gameEngine.getActivePlayers().size() > 2) {
                final Player currentPlayer = gameEngine.getCurrentPlayer();
                if (!currentPlayer.isFinished()) {
                    if (playerState == START_ROUND) {
                        if (graphicOn) {
                            slowDown(delayMS);
                        }
                        startRound(currentPlayer);
                    } else if (playerState == WAITING_TO_CHOOSE) {
                        choosingCard(currentPlayer);
                    } else if (playerState == TAKE_A_CHANCE) {
                        takeChance(currentPlayer);
                        savePlayerMove(currentPlayer, gameEngine, null, TAKE_A_CHANCE);
                    } else if (playerState == DRAW) {
                        chooseDraw(currentPlayer);
                        savePlayerMove(currentPlayer, gameEngine, null, DRAW);
                    } else {
                        finishRound(currentPlayer);
                    }
                } else {
                    skipPlayer(currentPlayer);
                }
                if (currentPlayer.isFinished() && !playerStatsEndPostions.containsKey(currentPlayer.getId())) {
                    playerStatsEndPostions.put(currentPlayer.getId(), placement);
                    placement++;
                }
            } else {
                if (gameEngine.getActivePlayers().size() <= 2) {
                    playerStatsEndPostions.put(gameEngine.getActivePlayers().get(0).getId(), 2);
                    playerStatsEndPostions.put(gameEngine.getActivePlayers().get(1).getId(), 3);
                    stats.add(new Stats(playerStatsEndPostions, (gameEngine.getNumberOfRounds())));
                }
                gameRunning = false;
            }

            if (graphicOn) {
                updatePlayerCardsPosition();
                playerMouseOver();
                playerMouseInput();
                playerKeyboardInput();
                draw();
                glfwPollEvents();
            }
        }
    }

    private void choosingCard(final Player currentPlayer) {
        if (currentPlayer instanceof HumanPlayerImpl) {
            if (null != cardToUse) {
                if (CardRulesUtil.cardsCanBeUsed(gameEngine.getOpenDeck().topCard(), Collections.singletonList(cardToUse))) {
                    savePlayerMove(currentPlayer, gameEngine, cardToUse, PLAY_CARD);
                    final boolean anotherTurn = gameEngine.playCard(currentPlayer, Collections.singletonList(cardToUse), false);
                    if (anotherTurn && !currentPlayer.isFinished()) {
                        gameEngine.drawCard(currentPlayer);
                        playerState = WAITING_TO_CHOOSE;
                    } else {
                        playerState = FINISHED;
                    }
                } else {
                    // Could not use card //
                    playerState = WAITING_TO_CHOOSE;
                    System.out.println("Cannot play card " + cardToUse);
                }
            }
            cardToUse = null;
        } else { // AI
            final AiPlayer ai = (AiPlayer) currentPlayer;
            final List<Card> cardToUse = ai.tryToUseCard(gameEngine);
            if (cardToUse.isEmpty() || ai.wantToDrawCards(gameEngine) && gameEngine.getOpenDeck().hasCards()) {
                playerState = DRAW;
            } else if (cardToUse.isEmpty() || ai.decideToTakeAChange(gameEngine) && gameEngine.getClosedDeck().hasCards()) {
                playerState = TAKE_A_CHANCE;
            } else {
                if (CardRulesUtil.cardsCanBeUsed(gameEngine.getOpenDeck().topCard(), cardToUse)) {
                    savePlayerMove(currentPlayer, gameEngine, cardToUse.get(0), PLAY_CARD);
                    final boolean anotherTurn = gameEngine.playCard(currentPlayer, Collections.singletonList(cardToUse.get(0)), false);
                    if (anotherTurn && !currentPlayer.isFinished()) {
                        gameEngine.drawCard(currentPlayer);
                        playerState = WAITING_TO_CHOOSE;
                    } else {
                        playerState = FINISHED;
                    }
                } else {
                    playerState = DRAW;
                }
            }
        }
    }

    private void draw() {
        graphicEngine.preDraw();
        graphicEngine.drawBackground();
//        graphicEngine.drawDesign();
        graphicEngine.drawTableCards(gameEngine.getTopCard(), gameEngine.getClosedDeck().hasCards());
        for (final PlayerBase player : gameEngine.getAllPlayers()) {
            graphicEngine.drawPlayerCards(player.getAllCards());
        }
        graphicEngine.drawHighlightedCard(highlightedCard);
        graphicEngine.drawActions(actionList);
        graphicEngine.postDraw();
    }

    private void displayStats() {
        final Map<Integer, Integer> numberOfTimesPlayerWon = new HashMap<>();
        if (!stats.isEmpty()) {
            for (final Stats stat : stats) {
                if (numberOfTimesPlayerWon.containsKey(stat.getFirstPlaceId())) {
                    int antallSeier = numberOfTimesPlayerWon.get(stat.getFirstPlaceId());
                    numberOfTimesPlayerWon.put(stat.getFirstPlaceId(), antallSeier + 1);
                } else {
                    numberOfTimesPlayerWon.put(stat.getFirstPlaceId(), 1);
                }
            }

            System.out.println("");
            System.out.println("Vinner liste:");
            for (final Map.Entry<Integer, Integer> playerEntry : numberOfTimesPlayerWon.entrySet()) {
                for (final PlayerBase player : players) {
                    if (player.getId() == playerEntry.getKey()) {
                        System.out.println("Spiller " + player.name() + " har vunnet: " + playerEntry.getValue());
                        break;
                    }
                }
            }
        } else {
            System.out.println("Ingen vinnere");
        }
    }

    private void updatePlayerCardsPosition() {
        for (final Player player : gameEngine.getAllPlayers()) {
            player.updateCardPosition();
        }
    }

    private void slowDown(final int msTime) {
        try {
            Thread.sleep(msTime);
        } catch (final InterruptedException e) {
            System.out.println(e);
        }
    }

    private void startRound(final Player currentPlayer) {
        if (currentPlayer.isFinished()) {
            playerState = FINISHED;
            return;
        }
        final boolean hasToDraw = gameEngine.checkIfPlayerMustDraw(currentPlayer);
        if (hasToDraw) {
            playerState = FINISHED;
        } else {
            playerState = WAITING_TO_CHOOSE;
        }
    }

    private void savePlayerMove(final Player player, final GameEngine gameEngine, final Card cardUsed, final PlayerState playerState) {
        /*
        final String[] trainingData = AIDataConverter.createDataForTraining(player.getAllCards(), gameEngine, cardUsed, playerState);
        if (playerMoves.containsKey(player.getId())) {
            final List<String[]> list = playerMoves.get(player.getId());
            list.add(trainingData);
            playerMoves.put(player.getId(), list);
        } else {
            final List<String[]> list = new ArrayList<>();
            list.add(trainingData);
            playerMoves.put(player.getId(), list);
        }
         */
    }

    private void takeChance(final Player currentPlayer) {
        if (gameEngine.getClosedDeck().hasCards()) {
            final boolean anotherTurn = gameEngine.takeAChange(currentPlayer);
            if (anotherTurn) {
                playerState = WAITING_TO_CHOOSE;
            } else {
                playerState = FINISHED;
            }
        } else {
            playerState = WAITING_TO_CHOOSE;
        }
    }

    private void chooseDraw(final Player currentPlayer) {
        if (gameEngine.getOpenDeck().hasCards()) {
            currentPlayer.giveCards(gameEngine.getOpenDeck().drawAllCards());
            playerState = FINISHED;
        } else {
            playerState = WAITING_TO_CHOOSE;
        }
    }

    private void finishRound(final Player currentPlayer) {
        gameEngine.drawCard(currentPlayer);
        gameEngine.nextPlayer();
        playerState = START_ROUND;
        gameEngine.increaseNumberOfRounds(currentPlayer);
    }

    private void skipPlayer(final Player currentPlayer) {
        gameEngine.nextPlayer();
        playerState = START_ROUND;
        gameEngine.increaseNumberOfRounds(currentPlayer);
    }

    private Cursor getCursorPos(long windowID) {
        final DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
        final DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
        glfwGetCursorPos(windowID, xBuffer, yBuffer);

        float normalizedX = (float) (-1.0 + 2.0 * xBuffer.get(0) / graphicEngine.getWindowWidth());
        float normalizedY = (float) (1.0 - 2.0 * yBuffer.get(0) / graphicEngine.getWindowHeight());
        return new Cursor(normalizedX, normalizedY);
    }

    private void playerMouseOver() {
        glfwSetCursorPosCallback(graphicEngine.getWindow(), new GLFWCursorPosCallback() {
            @Override
            public void invoke(long l, double v, double v1) {
                highlightedCard = null;
                if (null != gameEngine.getHumanPlayer()) {
                    final Cursor cursor = getCursorPos(graphicEngine.getWindow());
                    for (final Component component : gameEngine.getHumanPlayer().getAllCards()) {
                        if (pointInsideSquare(component, cursor)) {
                            highlightedCard = (Card) component;
                        }
                    }
                }
            }
        });
    }

    private void playerMouseInput() {
        glfwSetMouseButtonCallback(graphicEngine.getWindow(), new GLFWMouseButtonCallback() {
            @Override
            public void invoke(long window, int button, int action, int mods) {
                if (playerState == WAITING_TO_CHOOSE && button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                    final Cursor cursor = getCursorPos(window);
                    for (final Component component : actionList) {
                        if (pointInsideSquare(component, cursor)) {
                            playerState = component.getPlayerState();
                        }
                    }

                    for (final Component component : gameEngine.getHumanPlayer().getAllCards()) {
                        if (pointInsideSquare(component, cursor)) {
                            System.out.println("Clicked " + component);
                            cardToUse = (Card) component;
                            highlightedCard = null;
                        }
                    }
                }
            }
        });
    }

    private void playerKeyboardInput() {
        if (null != gameEngine.getHumanPlayer()) {
            int startKey = 49;
            glfwSetKeyCallback(graphicEngine.getWindow(), (window, key, scancode, action, mods) -> {
                // exit //
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                    gameRunning = false;
                }
                // drawCards cards //
                if (playerState == WAITING_TO_CHOOSE && key == GLFW_KEY_D && action == GLFW_RELEASE) {
                    playerState = DRAW;
                }

                // take a chance //
                if (playerState == WAITING_TO_CHOOSE && key == GLFW_KEY_C && action == GLFW_RELEASE) {
                    playerState = TAKE_A_CHANCE;
                }

                if (playerState == WAITING_TO_CHOOSE && action == GLFW_RELEASE &&
                        (key - startKey + 1 <= gameEngine.getHumanPlayer().getAllCards().size()) && key - startKey >= 0) {
                    cardToUse = gameEngine.getHumanPlayer().getAllCards().get(key - startKey);
                }
            });
        }
    }

    private void terminateGame() throws IOException {
        NnAiUtil.terminate();
        NnAiUtilV2.terminate();
        if (graphicOn) {
            glfwSetWindowShouldClose(graphicEngine.getWindow(), true);
            graphicEngine.terminate();
        }
    }
}
