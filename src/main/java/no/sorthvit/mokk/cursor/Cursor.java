package no.sorthvit.mokk.cursor;

public class Cursor {
    final private float postionX;
    final private float positionY;

    public Cursor(float postionX, float positionY) {
        this.postionX = postionX;
        this.positionY = positionY;
    }

    public float getPositionX() {
        return postionX;
    }

    public float getPositionY() {
        return positionY;
    }
}
