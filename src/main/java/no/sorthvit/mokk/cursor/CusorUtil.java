package no.sorthvit.mokk.cursor;

import no.sorthvit.mokk.gui.Component;

public class CusorUtil {
    public static boolean pointInsideSquare(final Component component, final Cursor cursor) {
        if (component.drawHorizontal()) {
            return pointInsideSquare(
                    component.getPositionX(),
                    component.getPositionY() - component.getHeight(),
                    component.getPositionX() + component.getWidth(),
                    component.getPositionY(),
                    cursor.getPositionX(),
                    cursor.getPositionY()
            );
        } else {
            return pointInsideSquare(
                    component.getPositionX(),
                    component.getPositionY() - component.getWidth(),
                    component.getPositionX() + component.getHeight(),
                    component.getPositionY(),
                    cursor.getPositionX(),
                    cursor.getPositionY()
            );
        }
    }

    public static boolean pointInsideSquare(float x1, float y1, float x2,
                                       float y2, float x, float y) {
        return x > x1 && x < x2 && y > y1 && y < y2;
    }
}
