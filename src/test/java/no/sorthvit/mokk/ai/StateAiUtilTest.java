package no.sorthvit.mokk.ai;

import no.sorthvit.mokk.ai.stateful.StateAiUtil;
import no.sorthvit.mokk.game.deck.Card;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StateAiUtilTest {

    @Test
    public void ai_find_lowest_allowed_card_empty_table() {
        final List<Card> cardList = new ArrayList<>();
        cardList.add(new Card(10, 2, false));
        cardList.add(new Card(11, 2, false));
        cardList.add(new Card(12, 2, false));
        cardList.add(new Card(3, 2, false));
        cardList.add(new Card(5, 2, false));

        final List<Card> decidedCard = StateAiUtil.useLowestLegal( null, cardList);
        Assert.assertEquals(3, decidedCard.get(0).getValue());
    }

    @Test
    public void ai_find_lowest_allowed_card_defined_table() {
        final List<Card> cardList = new ArrayList<>();
        cardList.add(new Card(10, 2, false));
        cardList.add(new Card(11, 2, false));
        cardList.add(new Card(12, 2, false));
        cardList.add(new Card(3, 2, false));
        cardList.add(new Card(5, 2, false));

        final List<Card> decidedCard = StateAiUtil.useLowestLegal(new Card(11, 1, false), cardList);
        Assert.assertEquals(11, decidedCard.get(0).getValue());
    }

    @Test
    public void ai_find_lowest_allowed_card_vs_high_card_on_table() {
        final List<Card> cardList = new ArrayList<>();
        cardList.add(new Card(10, 2, false));
        cardList.add(new Card(11, 2, false));
        cardList.add(new Card(12, 2, false));
        cardList.add(new Card(3, 2, false));
        cardList.add(new Card(5, 2, false));

        final List<Card> decidedCard = StateAiUtil.useLowestLegal(new Card(13, 3, false), cardList);
        Assert.assertEquals(0, decidedCard.size());
    }


    @Test
    public void ai_find_lowest_allowed_card_which_is_even() {
        final List<Card> cardList = new ArrayList<>();
        cardList.add(new Card(8, 2, false));
        cardList.add(new Card(9, 2, false));
        cardList.add(new Card(11, 2, false));
        cardList.add(new Card(12, 2, false));

        final List<Card> decidedCard = StateAiUtil.useLowestLegal(new Card(9, 3, false), cardList);
        Assert.assertEquals(9, decidedCard.get(0).getValue());
    }

//    @Test
//    public void random_take_a_change() {
//        final ArrayList<Boolean> aiChoices = new ArrayList<>();
//        for(int i = 0; i < 10; i++){
//            final Boolean choice = AiUtil.decideToTakeAChange(new AiPlayerPlayerImpl("", 0, 0, false));
//            System.out.println("AiPlayer choice : " + choice);
//            aiChoices.add(choice);
//        }
//        Assert.assertTrue(aiChoices.contains(Boolean.TRUE));
//        Assert.assertTrue(aiChoices.contains(Boolean.FALSE));
//    }

    @Test
    public void ai_decide_after_using_ten() {
        final List<Card> cardList = new ArrayList<>();
        cardList.add(new Card(8, 2, false));
        cardList.add(new Card(9, 2, false));
        cardList.add(new Card(10, 2, false));
        cardList.add(new Card(11, 2, false));
        cardList.add(new Card(12, 2, false));

        final List<Card> decidedCard = StateAiUtil.useLowestLegal(null, cardList);
        Assert.assertEquals(8, decidedCard.get(0).getValue());
    }
}
