package no.sorthvit.mokk.util;

import no.sorthvit.mokk.game.deck.Card;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CardRulesUtilTest {
    @Test
    public void card_on_table_but_non_to_use() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(15, 1, false), null);
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void non_card_on_table() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(null, new Card(15, 1, false));
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void high_card_on_table_low_to_use() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(10, 1, false), new Card(5, 1, false));
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void low_card_on_table_high_to_use() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(5, 1, false), new Card(10, 1, false));
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void even_cards() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(5, 1, false), new Card(5, 2, false));
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void ace_on_below_ten() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(5, 1, false), new Card(14, 2, false));
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void ace_on_higher_than_ten() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(11, 1, false), new Card(14, 2, false));
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void ace_on_2() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(15, 1, false), new Card(14, 2, false));
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void ten_on_below_ten() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(9, 1, false), new Card(10, 2, false));
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void ten_on_higher_than_ten() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(11, 1, false), new Card(10, 2, false));
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void ten_on_2() {
        final boolean canBeUsed = CardRulesUtil.cardCanBeUsed(new Card(15, 1, false), new Card(10, 2, false));
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void non_card_on_table_use_three() {
        final List<Card> cardsToUse = new ArrayList<>();
        cardsToUse.add(new Card(3, 1, false));
        cardsToUse.add(new Card(3, 2, false));
        cardsToUse.add(new Card(3, 3,false));

        final boolean canBeUsed = CardRulesUtil.cardsCanBeUsed(null, cardsToUse);
        Assert.assertTrue(canBeUsed);
    }
}
