# README
Simple card game called dirt, with lwjgl, two different ai types, made as a hobby project.
- Max 4 players
- Minimum 2 players

### Types of players
- ApplicationWithGui.class add players to resetGame();
- HumanPlayerImpl.class (playable human)
- NNAIPlayer.class (ai nn player)
- StateAiUtil (Simple stateful ai)

### Settings
- useCheats = see opponents cards
- TOTAL_GAMES = number of games to play
- graphicOn = turn on opengl graphics and inputs
- delayMS = delay between players

### Input for humans
- Use mouse and click on card to use.
- click on action-button draw to draw cards on table.
- click on action-button take a chance, to play the top card. If it fails, you get the cards.
- Keyboard (1 - 9) for use card
- Keyboard (d) to draw cards
- Keyboard (c) to take a chance

### Game rules
- Can only use on card
- 10 erase pile if cards are below 10 and pile is not empty and give a second round for the player
- Ace erase pile if cards are above 10 and pile is not empty and give a second round for the player
- 2 is the strongest card and will always be in play. Can never be erased by 10 or ace.

### Copyright
Open copywrite protection, Enjoy for own usage
- Kenneth Isaksen